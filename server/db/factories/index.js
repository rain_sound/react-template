const faker = require("faker");
const bcrypt = require("bcryptjs")
const { User, Post } = require("../../db");

var i = 0;
for(i = 0; i < 15; i++){
    var pass = faker.random.word();
    var name = faker.name.firstName();
    console.log('Name = ',name,'Password ==', pass)
    User.create({
        name: name,
        email: faker.internet.email(),
        imgProfile: 'default.png',
        password: bcrypt.hashSync(pass, 8)
    })
}

i = 0;
for(i = 0; i < 15; i++){
    Post.create({
        userId: 1,
        deleted: false,
        head: faker.lorem.word(),
        body: faker.lorem.paragraph()
    })
}