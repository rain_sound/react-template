module.exports = (sequelize, type) => {
    return sequelize.define('users', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: type.STRING(50),
            unique: {
                args: true,
                msg: 'Ya existe una categoría con ese nombre.',
                fields: ['name']
            },    
        },
        email: {
            type: type.STRING(150),
            unique: {
                msg: 'This email is already taken.',
                fields: ['email']
            }
        },
        password: type.STRING(255),
        passwordResetToken: type.STRING(60),
        passwordResetTokenExpire: type.DATE,
        imgProfile: type.STRING(255)
    })
}