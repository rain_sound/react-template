const Sequelize = require('sequelize')
const UserModel = require('./models/user.model')
    // const EntradaModel = require('./models/entrada.model')
const cf = require('../config')

//Configuracion inicial
const conn = new Sequelize(cf.db_name, cf.user_db, cf.password_db, {
    host: cf.host,
    dialect: cf.dialect,
    pool: {
        max: cf.max_conn,
        min: cf.min_conn
    }
})

//Iniciar modelos | ORM 
const User = UserModel(conn, Sequelize)

// User.belongsTo(Role)

//Force === true Para crear todas las tablas cuidado dropea toda la data
//Force === false Para crear todas las tablas que no existe

conn.sync({ force: false })
    .then(() => {
        console.log('Tablas y database creados')
    })
    .catch(err => {
        console.log(err);
    })

module.exports = {
    User,
}