var router = require('express').Router()
    //Definir rutas
const users = require('./users')


//Controladores
router.use('/users', users)

module.exports = router