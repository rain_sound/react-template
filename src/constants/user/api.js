import { URL } from '../../helpers/api'

const SIGNIN            = `${URL}users/signin` 
const SIGNUP            = `${URL}users/signup` 
const FORGOTPASSWORD    = `${URL}users/forgotpassword` 
const RESETPASSWORD     = `${URL}users/resetpassword` 
const UPDATEPASSWORD    = `${URL}users/updatepassword` 
const UPDATEIMAGE       = `${URL}users/uploadImage` 

export const userConstants = {
    SIGNIN,
    SIGNUP,
    FORGOTPASSWORD,
    RESETPASSWORD,
    UPDATEPASSWORD,
    UPDATEIMAGE
}