import { store } from '../store/index'

export const createToast = options => {
  var len = (store.getState().alert)['toast'];
  return {
    ...options,
    id: len
  }
}

