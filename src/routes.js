import React from 'react';
import { store } from './store'

const Dashboard = React.lazy(() => import('./views/Dashboard/Dashboard'));
const Profile = React.lazy(() => import('./components/Profile/Profile'));
const PasswordRecovery = React.lazy(() => import('./components/Profile/PasswordRecovery'));

const user = store.getState().user;

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/profile', exact:true, name: 'Profile', component: PasswordRecovery },
  { path: '/profile/:name/reset', name: 'Reset Password', component: PasswordRecovery },
  { path: '/profile/:name', name: 'Profile Image', component: Profile },

];

export default routes;
