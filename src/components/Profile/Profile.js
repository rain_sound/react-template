import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import CardInfo from "../Other/CardInfo";
import CardImage from "../Other/CardImage";

export class Profile extends Component {
  constructor() {
    super();
    this.state = {
      submited: false,
      newPassword: "",
      oldPassword: "",
      confirmPassword: ""
    };
  }

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };


  renderProfile = () => {
    return (
      <form onSubmit={this.handleUpdate}>
        <div className="form-group form-row">
          <label>Usuario</label>
          <input
            type="text"
            name="username"
            id="username"
            disabled
            className="form-control"
            value={this.props.user.name}
          />
        </div>
        <div className="form-group form-row">
          <label>Email</label>
          <input
            type="email"
            name="email"
            id="email"
            disabled
            className="form-control"
            value={this.props.user.email}
          />
        </div>
        <CardImage />
      </form>
    );
  };
  render() {
    return (
      <div className="row justify-content-center">
        <CardInfo
          body={this.renderProfile()}
          title="Configuración de Perfil"
          width="8"
        />
      </div>
    );
  }
}

const mapStateToProps = ({ user }) => ({ user });

Profile.prototypes = {
  user: PropTypes.shape({
    name: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    imgProfile: PropTypes.string.isRequired,
    reason: PropTypes.string,
    message: PropTypes.string
  })
};

export default connect(mapStateToProps)(Profile);
