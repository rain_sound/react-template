import UpdateImage from './Profile'
import PasswordRecovery from './PasswordRecovery'

export const profile = {
    UpdateImage,
    PasswordRecovery
}