import { combineReducers } from 'redux'
import { user, getUserId as _getUserId, getUsername as _getUsename } from './user'
import { alert } from './alert'
import { posts, postSeleted, commentsList, getImgPost as _getImgPost } from './entrada'
import { userConstants } from '../constants/user/actions'

 const appReducer = combineReducers({
    user,
    posts,
    postSeleted,
    commentsList,
    alert
})

export const rootReducer = (state, action) => {
    if (action.type === userConstants.LOGOUT) {
        state = undefined
      }
    
      return appReducer(state, action)
  }
//Selectors
export const getUserId = state => _getUserId(state.user)
export const getUsername = state => _getUsename(state.user)
export const getListPosts = state => (state.posts)
export const getSelectedPost = state => (state.postSeleted)
export const getCommentsList = state => (state.commentsList)
export const getImgPost = state => _getImgPost(state.postSeleted)